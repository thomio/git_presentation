
## README

This presentation introduces Git to new users.  
It covers the basics and tries to be as practical as possible.  
The concepts are applicable to any Operating System but some bash commands are used.


## BUILD

To generate the presentation you have to compile it using pdflatex.

On Ubuntu you can type in terminal:

```bash
  $ pdflatex presentation.tex
```

pdflatex can be found in the texlive-latex-base package:

```bash
  $ sudo apt-get install texlive-latex-base
```


Copyright © 2016 Thomio Watanabe